import numpy as np
import translacja
import transkrypcja
import fraction

# tablica podstawień dla nici wiodącej, jako argumenty przyjmuje sekwencję genową (gene) do której przypisywany jest nukleotyd
# która następnie jest zwracana, prob - wylosowana liczba z przedzialu [0, 1] oraz nukleotyd który podlega mutacji
def matrix_for_leading_strand(gene, prob, nucleotide):
    if (nucleotide == 'A'):
        if (prob <= 0.023):
            gene += 'C'
        elif (prob <= 0.09):
            gene += 'G'
        elif (prob <= 0.193):
            gene += 'T'
        else:
            gene += 'A'
    elif (nucleotide == 'T'):
        if (prob <= 0.065):
            gene += 'A'
        elif (prob <= 0.1):
            gene += 'G'
        elif (prob <= 0.135):
            gene += 'C'
        else:
            gene += 'T'
    elif (nucleotide == 'G'):
        if (prob <= 0.164):
            gene += 'A'
        elif (prob <= 0.28):
            gene += 'T'
        elif (prob <= 0.295):
            gene += 'C'
        else:
            gene += 'G'
    elif (nucleotide == 'C'):
        if (prob <= 0.070):
            gene += 'A'
        elif (prob <= 0.331):
            gene += 'T'
        elif (prob <= 0.378):
            gene += 'G'
        else:
            gene += 'C'
    else:
        gene += nucleotide

    return gene

# tablica podstawień dla nici opoźnionej, argumenty jak wyżej
def matrix_for_lagging_strand(gene, prob, nucleotide):
    if (nucleotide == 'A'):
        if (prob <= 0.035):
            gene += 'C'
        elif (prob <= 0.07):
            gene += 'G'
        elif (prob <= 0.135):
            gene += 'T'
        else:
            gene += 'A'
    elif (nucleotide == 'T'):
        if (prob <= 0.103):
            gene += 'A'
        elif (prob <= 0.126):
            gene += 'G'
        elif (prob <= 0.193):
            gene += 'C'
        else:
            gene += 'T'
    elif (nucleotide == 'G'):
        if (prob <= 0.261):
            gene += 'A'
        elif (prob <= 0.331):
            gene += 'T'
        elif (prob <= 0.378):
            gene += 'C'
        else:
            gene += 'G'
    elif (nucleotide == 'C'):
        if (prob <= 0.116):
            gene += 'A'
        elif (prob <= 0.28):
            gene += 'T'
        elif (prob <= 0.295):
            gene += 'G'
        else:
            gene += 'C'
    else:
        gene += nucleotide

    return gene

# funkcja wykonująca substytucje na podanej sekwencji nukleotydowej
# argumenty to sekwencja oraz znak nici na której zapisany jest gen
def substitution(line, strand):
    gene = ''
    for nucleotide in line:
        # losujemy liczbę z przedziału [0, 1] jeśli jest mniejsza od 0.01 następuje substytucja
        if (np.random.rand() < 0.01):
            prob = np.random.rand()
            # zmieniamy na dany nukleotyd według tabeli substytucji
            if (strand == '+'):
                # tabela dla nici wiodącej
                gene = matrix_for_leading_strand(gene, prob, nucleotide)
            elif (strand == '-'):
                # tabela dla nici opożnionej
                gene = matrix_for_lagging_strand(gene, prob, nucleotide)
        # jeśli nie następuje mutacja nukleotyd jest dopisywany do sekwencji bez zmian
        else:
            gene += nucleotide
    gene = gene.strip()
    # zwracany jest gen poddany mechanizmom mutacji
    return gene

# funkcja obliczająca parametr T
def tolerance(result_original, result):
    aminoacid = ['A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y']
    t = 0
    # dla każdego aminokwasu obliczana jest różnica statystyki występowania między sekwencją originalną oraz sekwencją po mutacji
    for amin in aminoacid:
        t += abs(result_original[amin] - result[amin])
    # zwracana jest suma wszystkich obliczonych różnic
    return t

# funkcja poddająca mutacji każdy gen z pliku (poprzez wywołanie funkcji substytucji)
def mutation(file_read, file_write, tolerance_parametr):
    # otwarcie pliku z aktualną populacją
    with open(file_read, 'rt') as file_r:
        lines_r = file_r.readlines()
        # otwarcie pliku ze statystyką wystepowania aminokwasów w sekwencji oryginalnej populacji
        with open('fraction_original.txt', 'rt') as file_fraction:
            lines_f = file_fraction.readlines()
            # otwarcie pliku do którego zostanie zapisana utworzona populacja
            with open(file_write, 'w') as file_w:
                number_of_gene = 0
                number_of_accepted = 0
                number_of_killed = 0

                # odczyt genów z populacji wejściowej
                for line_r in lines_r:
                    if (line_r.startswith('>')):
                        # zapisanie numeru genu oraz znaku nici
                        number_of_gene = int(line_r[5:-2])
                        strand = line_r[-2]
                    else:
                        line_r = line_r.strip()
                        # wykonanie substytucji na sekwencji aktualnego genu
                        gene = substitution(line_r, strand)
                        # przepisanie zmutowanego genu na rna
                        rna = transkrypcja.transkrypcja(gene)
                        # przepisanie na białko
                        protein = translacja.translacja(rna)
                        # zliczenie statystyki występowania aminokwasów
                        result = fraction.fraction(protein)

                        # pętla po frakcjach oryginalnych
                        for index, line_f in enumerate(lines_f):
                            # poszukiwanie statystyki dla odpowiedniego genu
                            if (line_f.startswith('>result'+str(number_of_gene))):
                                # obliczanie wartości T dla statystyki oryginalnej oraz statyski obliczonej dla zmutowanego genu
                                t = tolerance(eval(lines_f[index+1]), result)
                                if (t < tolerance_parametr):
                                    # gen jest zaakceptowany
                                    number_of_accepted += 1
                                    # zapisanie zmutowanego genu do nowej populacji
                                    file_w.write('>gene' + str(number_of_gene) + ' ' + strand + '\n' + gene + '\n')
                                    break
                                else:
                                    # gen zostaje zabity i zastąpiony przez gen o tym samym numerze z populacji poprzedniej
                                    number_of_killed += 1
                                    # petla po populacji z kroku poprzedniego
                                    for index2, line_r2 in enumerate(lines_r):
                                        # szukamy genu w populacji, który podlegał mutacji
                                        if (line_r2.startswith('>gene' + str(number_of_gene))):
                                            # zapisujemy do populacji gen z poprzedniej populacji
                                            file_w.write('>gene' + str(number_of_gene) + ' ' + strand + '\n' + lines_r[index2+1])
                                            break
                                    break

    # zwracamy liczbę zaakceptowanych i liczbę odrzuconych genów
    return number_of_accepted, number_of_killed

