import numpy as np

# funkcja tworząca losowy genom o dlugosci 900000
def random_genome():
    with open('random_genome.txt', 'w') as file:
        dna = ''
        nucleotid = ['A', 'T', 'G', 'C']
        for i in range(0, 900000):
            number = int(np.random.rand()*4)
            # losowo wybieramy nukleotyd i zapisujemy go do sekwencji
            dna += nucleotid[number]
        file.write(dna)

# funkcja pobierająca nukleotydy zawarte w prawdziwej sekwencji i rozmieszczajaca je losowo w nowym genomie
def random_nucleotid():
    count = {'A': 0, 'C': 0, 'G': 0, 'T': 0}
    # zliczamy występowanie poszczególnych nukleotydów w sekwencji
    with open('../escherichia_coli/escherichia_coli.fna', 'rt') as file:
        for line in file.readlines():
            for nucleotid in line.strip():
                if (nucleotid in count):
                    count[nucleotid] += 1
    nucleotids = ['A', 'T', 'G', 'C']
    dna = ''

    with open('random_nucleotid.txt', 'w') as file:
        # dopóki nie wykrzystamy wszystkich nukleotydów wydłużamy sekwencje
        while not all(value == 0 for value in count.values()):
            # losujemy nukleotyd
            number = int(np.random.rand() * 4)
            nucleotid = nucleotids[number]
            # jeżeli nie wykorzystaliśmy jeszcze ilości wylosowanego nukloetydu to dopisujemy go do sekwencji
            if (count[nucleotid] != 0):
                dna += nucleotid
                # zmniejszamy dostępną ilość tego nukletydu
                count[nucleotid] -= 1
        file.write(dna)

# random_genome()
random_nucleotid()
