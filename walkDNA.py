import matplotlib.pyplot as plt

# funkcja tworzaca wykres przejścia po DNA
def walkDNA(file):
    # tablice przechowujace położenie funkcji na osi x oraz na osi y
    x_axisAT = [0]
    y_axisAT = [0]
    x_axisGC = [0]
    y_axisGC = [0]
    x = 0
    # czytanie kazdego nukleotydu z sekwencji czystego genomu
    with open(file, 'rt') as file:
        for line in file.readlines():
            line = line.strip()
            for nucleotide in line:
                # z kazdym przeczytanym nukleotydem zwiekszamy x aby przemieszczac sie na osi x
                x += 1
                # dla osi y pobieramy aktualna wartość na której sie znajdujemy i dodajemy lub
                # odejmujemy jeden w zależności od podanych kryteriów
                if (nucleotide == 'A'):
                    x_axisAT.append(x)
                    y_axisAT.append(y_axisAT[-1] + 1)
                    x_axisGC.append(x)
                    y_axisGC.append(y_axisGC[-1])
                elif (nucleotide == 'T'):
                    x_axisAT.append(x)
                    y_axisAT.append(y_axisAT[-1] - 1)
                    x_axisGC.append(x)
                    y_axisGC.append(y_axisGC[-1])
                elif (nucleotide == 'G'):
                    x_axisAT.append(x)
                    y_axisAT.append(y_axisAT[-1])
                    x_axisGC.append(x)
                    y_axisGC.append(y_axisGC[-1] + 1)
                elif (nucleotide == 'C'):
                    x_axisAT.append(x)
                    y_axisAT.append(y_axisAT[-1])
                    x_axisGC.append(x)
                    y_axisGC.append(y_axisGC[-1] - 1)
    # zwracamy tablice przejścia dla osi x oraz y
    return x_axisAT, y_axisAT, x_axisGC, y_axisGC

# funkcja tworząca wykres przejścia po DNA
def draw(file):
    x_axisAT, y_axisAT, x_axisGC, y_axisGC = walkDNA(file)
    plt.plot(x_axisAT, y_axisAT, label='AT')
    plt.plot(x_axisGC, y_axisGC, label='GC')
    plt.show()

draw('clear_genome.txt')

