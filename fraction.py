# funkcja zliczajaca wystapienia aminokwasu w białku
# oraz podajaca część jaką stanowi ten aminokwas
def fraction(protein):
    dict_sum = {
                'A':0, 'C':0, 'D':0, 'E':0,
                'F':0, 'G':0, 'H':0, 'I':0,
                'K':0, 'L':0, 'M':0, 'N':0,
                'P':0, 'Q':0, 'R':0, 'S':0,
                'T':0, 'V':0, 'W':0, 'Y':0
                }

    # zliczanie aminokwasów
    for amino in protein:
        if not (amino == '_'):
            dict_sum[amino] += 1

    dict_fraction = {}
    length = len(protein)
    # zliczanie frakcji
    for value in dict_sum:
        # tworzony jest nowy słownik zawierający wartosci ze słownika
        # sumy aminokwasów podzielone przez długość białka
        dict_fraction[value] = dict_sum[value]/length

    return dict_fraction

# funkcja obliczająca wystapienia aminokwasów w każdym genie
def fraction_genes():
    with open('protein.txt', 'rt') as file:
        with open('fraction_original.txt', 'w') as file2:
            for line in file.readlines():
                if (line.startswith('>')):
                    number = line[8:11]
                    strand = line[-2]
                else:
                    line = line.strip()
                    # dla każdego białka z pliku wywołujemy funkcje zliczania aminokwasów
                    count = fraction(line)
                    # wynik zapisujemy do pliku
                    file2.write('>result' + str(number) + ' ' + strand + '\n' + str(count) + '\n')
