import numpy

# funkcja przepisujaca podaną nić DNA na nić RNA dla podanego genu
def transkrypcja(dna):
    rna = ''
    # przepisujemy nić DNA zgodnie z podanymi regułami
    for nucleotide in dna:
        if (nucleotide == 'T'):
            rna += 'U'
        elif (nucleotide == 'W'):
            weak = ['A', 'U']
            rna += weak[int(numpy.random.rand() * 2)]
        elif (nucleotide == 'R'):
            purin = ['A', 'G']
            rna += purin[int(numpy.random.rand() * 2)]
        elif (nucleotide == 'K'):
            keto = ['U', 'C']
            rna += keto[int(numpy.random.rand() * 2)]
        elif (nucleotide == 'N'):
            anyNucleotide = ['A', 'U', 'G', 'C']
            rna += anyNucleotide[int(numpy.random.rand() * 4)]
        elif (nucleotide == 'M'):
            amino = ['A', 'C']
            rna += amino[int(numpy.random.rand() * 2)]
        elif (nucleotide == 'S'):
            strong = ['G', 'C']
            rna += strong[int(numpy.random.rand() * 2)]
        elif (nucleotide == 'Y'):
            pirymidine = ['U', 'C']
            rna += pirymidine[int(numpy.random.rand() * 2)]
        else:
            rna += nucleotide
    # zwracana jest sekwencja rna
    return rna

# funkcja otwierająca plik odczytu, wywołująca funkcje transkrypcji oraz zapisująca wynik do pliku
def writer_transkrypcja():
    with open('genes.txt', 'rt') as file:
        with open('genes_rna.txt', 'w') as file2:
            for line in file.readlines():
                # pomijamy linie zawierajaca numer genu
                if(line.startswith('>')):
                    number = line[5:8]
                    strand = line[-2]
                else:
                    rna = transkrypcja(line)
                    # zapisujemy do pliku podajac dodatkowo linie z numerem rna
                    file2.write('>gene_rna' + number + strand + '\n' + rna)




