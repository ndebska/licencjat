import genome
import transkrypcja
import translacja
import fraction
import mutation
import walkDNA
import matplotlib.pyplot as plt

# genome.edit_protein_table('clostridium_tetani/proteins.csv')
# tabFrom, tabTo, tabStrand = genome.create_infoTab('clostridium_tetani/dane_table.txt')
# genome.clearGenome_genes(tabFrom, tabTo, tabStrand, 'clostridium_tetani/Chlamydia_trachomatis.fasta')

# transkrypcja.writer_transkrypcja()
#
# translacja.writer_translacja()
#
# fraction.fraction_genes()


# funkcja wykonująca kroki Monte Carlo
def monteCarlo(tolerance_parametr):
    # pierwszy krok wykonujemy wychodząc z oryginalnej populacji genów, wyniki zapisujemy do pliku 'genes_after_mutation'
    number_accepted, number_killed = mutation.mutation('clostridium_tetani/genes.txt', 'clostridium_tetani/genes_after_mutation.txt', tolerance_parametr)
    number_MCs = [0]
    tab_accepted = []
    tab_killed = []

    tab_killed.append(number_killed)

    # określamy liczbę kroków
    for i in range(1500):
        print(number_MCs[i])
        # na zmianę geny pobieramy z dwóch plików aby nie tworzyć zmiennych przechowujących tak dużo informacji
        # oraz aby nie tworzyć większej ilości plików
        if not (i % 2 == 0):
            # wychodzimy z populacji zapisanej w genes_after_muattion2 i zapisujemy do genes_after_mutation
            number_accepted, number_killed = mutation.mutation('genes_after_mutation2.txt', 'genes_after_mutation.txt', tolerance_parametr)
            tab_killed.append(number_killed)
        else:
            # wychodzimy z populacji zapisanej w genes_after_muattion i zapisujemy do genes_after_mutation2
            number_accepted, number_killed = mutation.mutation('genes_after_mutation.txt', 'genes_after_mutation2.txt', tolerance_parametr)
            tab_killed.append(number_killed)

        number_MCs.append(number_MCs[i] + 1)

    # obliczamy jaki procent genów został odrzuconych
    for i in range(len(tab_killed)):
        tab_killed[i] = tab_killed[i] / len(tab_killed + tab_accepted)

    # przedstawienie wyników w formie wykresu
    plt.clf()
    x_axis = number_MCs
    y_axis = tab_killed
    plt.plot(x_axis, y_axis, label='accepted genes')
    # zapisanie wykresu
    plt.savefig('monteCarlo_simulation_Chlamydia_trachomatis' + '.png')

    # zapisanie końcowych populacji do plików
    file = 'population_from_simulation_Chlamydia_trachomatis' + '.txt'
    with open(file, 'w') as file_w:
        with open('genes_after_mutation2.txt', 'rt') as file_r:
            for line in file_r.readlines():
                file_w.write(line)

number_fig = 1;
tolerance_parametr = 0.3

# petla wykonujaca symulacje Monte Carlo w podanej ilości
for _ in range(1):
    monteCarlo(tolerance_parametr)
    number_fig += 1
    # tolerance_parametr += 0.2

# parametr = 0.8

genome.create_clear_genome('population_from_simulation_' + number_fig + '.txt', ' clear_genome_from_simulation' + '.txt')
walkDNA.draw('clear_genome_from_simulation' + '.txt')
