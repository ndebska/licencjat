import pandas as pd

# funkcja wycinająca z tabeli własciwości 3 kolumny i zapisująca je do nowego pliku
def edit_protein_table(file):
    feature_table = pd.read_csv(file)
    feature_table.index += 1
    feature_table = feature_table[['Start', 'Stop', 'Strand']]
    with open('dane_table.txt', 'w') as file:
        file.write(feature_table.to_string(header=False, index=True))

#funkcja pobierajaca z pliku dane o miejscu startu oraz zakonczenia genu
def create_infoTab(file_r):
    tabFrom = ''
    tabTo = ''
    tabStrand = ''

    with open(file_r, 'rt') as file:
        for line in file.readlines():
            tabFrom += line.split()[1] + ' '
            tabTo += line.split()[2] + ' '
            tabStrand += line.split()[3] + ' '

    # tworzenie tablic przechowujacych informacje o początku i końcu replikacji
    tabFrom = tabFrom.split()
    tabTo = tabTo.split()
    tabStrand = tabStrand.split()

    return tabFrom, tabTo, tabStrand

# funkcja tworzaca genom 'bez smieci' oraz połaczone ze soba poszczegolne geny
def clearGenome_genes(tabFrom, tabTo, tabStrand, file_r):
    genome = ''
    # pobieranie z pliku genomu i usuwanie bialych znaków
    with open(file_r, 'rt') as file:
        for line in file.readlines():
            genome += line.strip()

    # zapisywanie do pliku 'genes.txt' poszczególnych genów
    # oraz zapisywanie genów połączonych ze sobą czyli czystego genomu
    with open('genes.txt', 'w') as file:
        with open('clear_genome.txt', 'w') as f:
            for i in range(len(tabFrom)):
                gene = str(genome[int(tabFrom[i]) - 1: int(tabTo[i])])
                f.write(gene)
                if (tabStrand[i] == '+'):
                    gene = '>gene'+str(i+1) + '  ' + tabStrand[i] + '\n' + str(genome[int(tabFrom[i])-1: int(tabTo[i])]) + '\n'
                    file.write(gene)
                elif (tabStrand[i] == '-'):
                    gene_trans = ''
                    # odczytanie genu od tyłu
                    gene = gene[::-1]
                    for nucleoitide in gene:
                        if (nucleoitide == 'A'):
                            gene_trans += 'T'
                        elif (nucleoitide == 'T'):
                            gene_trans += 'A'
                        elif (nucleoitide == 'C'):
                            gene_trans += 'G'
                        elif (nucleoitide == 'G'):
                            gene_trans += 'C'
                        else:
                            gene_trans += nucleoitide
                    gene = '>gene' + str(i + 1) + '  ' + tabStrand[i] + '\n' + gene_trans + '\n'
                    file.write(gene)

def create_clear_genome(file_r, file_w):
    with open(file_r, 'rt') as file_read:
        with open(file_w, 'w') as file_write:
            for line in file_read.readlines():
                if not line.startswith('>'):
                    gene = line.strip()
                    file_write.write(gene);