# funkcja tłumaczaca nić RNA na białko
def translacja(rna):
    protein = ''
    rna = rna.strip()
    # kod genetyczny w formie słownika, klucze to kodony kodujace aminokwasy (wartosci)
    gencode = {
        'AUA': 'I', 'AUC': 'I', 'AUU': 'I', 'AUG': 'M',
        'ACA': 'T', 'ACC': 'T', 'ACG': 'T', 'ACU': 'T',
        'AAC': 'N', 'AAU': 'N', 'AAA': 'K', 'AAG': 'K',
        'AGC': 'S', 'AGU': 'S', 'AGA': 'R', 'AGG': 'R',
        'CUA': 'L', 'CUC': 'L', 'CUG': 'L', 'CUU': 'L',
        'CCA': 'P', 'CCC': 'P', 'CCG': 'P', 'CCU': 'P',
        'CAC': 'H', 'CAU': 'H', 'CAA': 'Q', 'CAG': 'Q',
        'CGA': 'R', 'CGC': 'R', 'CGG': 'R', 'CGU': 'R',
        'GUA': 'V', 'GUC': 'V', 'GUG': 'V', 'GUU': 'V',
        'GCA': 'A', 'GCC': 'A', 'GCG': 'A', 'GCU': 'A',
        'GAC': 'D', 'GAU': 'D', 'GAA': 'E', 'GAG': 'E',
        'GGA': 'G', 'GGC': 'G', 'GGG': 'G', 'GGU': 'G',
        'UCA': 'S', 'UCC': 'S', 'UCG': 'S', 'UCU': 'S',
        'UUC': 'F', 'UUU': 'F', 'UUA': 'L', 'UUG': 'L',
        'UAC': 'Y', 'UAU': 'Y', 'UAA': '_', 'UAG': '_',
        'UGC': 'C', 'UGU': 'C', 'UGA': '_', 'UGG': 'W'}

    # odczytywanie trójki nukleotydów z sekwencji aminokwasowej
    for i in range(0, len(rna), 3):
        kodon = rna[i: i + 3]
        # kodony inicjujące replikacje: AUG, CUG, UUG są tłumaczone na metioninę
        if (i == 0 and (kodon == 'UUG' or kodon == 'UUG' or kodon == 'AUU' or kodon == 'AUC' or kodon == 'AUA' or kodon == 'AUG' or kodon == 'GUG')):
            protein = 'M'
        else:
            protein += gencode[kodon]
    # zwracana jest sekwencje aminokwasów
    return protein

# funkcja otwierająca plik odczytu, wywołująca funkcje translacji oraz zapisująca wynik do pliku
def writer_translacja():
    # czytamy z pliku genów poszczególne kodony
    with open('genes_rna.txt', 'rt') as file:
        with open('protein.txt', 'w') as file2:
            number2 = 0
            for line in file.readlines():
                # z linii informacji o genie odczytujemy numer oraz znak nici
                if (line.startswith('>')):
                    number2 += 1
                    number = line[9:12]
                    strand = line[-2]
                else:
                    line = line.strip()
                    protein = translacja(line)
                    # zapisujemy przetłumaczone białko do pliku, dodatkowo numer genu oraz znak nici
                    file2.write('>protein' + number + strand + '\n' + protein + '\n')


